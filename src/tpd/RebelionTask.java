package tpd;


public class RebelionTask extends AbstractTask {

	private String rebelion;
	
	public RebelionTask ( String rebelion ) {
		this.rebelion = rebelion;
	}

	public void doRebelion () {
		println("American's firms announced that they rejected the use of blue.");
		println("They have opted instead for " + rebelion + ".");
	}
	
}
