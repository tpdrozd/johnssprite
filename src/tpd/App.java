package tpd;

import tpd.about.About;

/**
 * Main entry point.
 * 
 * @author Tomasz Drozd
 * @version 1.0.0 [Warszawa 14 mar 2016 13:36:01]
 */
public class App {
	
	public static void main ( String argv[] ) {
		System.out.println("Sprite 1.1.0");
		
		About.main();
		
		Task exampleTask = new Task("Example");
		exampleTask.done();
		
		NewsTask newsTask = new NewsTask("Blue will be this year's hot color.");
		newsTask.doNews();
		
		RainbowTask rainbowTask = new RainbowTask("RainBow");
		rainbowTask.doRainbow();
		
		RebelionTask rebelionTask = new RebelionTask("Red");
		rebelionTask.doRebelion();
		
		RedTask redTask = new RedTask();
		redTask.doRed();
		
		YellowTask yellowTask = new YellowTask();
		yellowTask.doYellow();
		
		GreenTask greenTask = new GreenTask();
		greenTask.doGreen();
		
		News3Task news3Task = new News3Task();
		news3Task.doNews3();
		
		PinkTask pinkTask = new PinkTask();
		pinkTask.doPink();
	}
	
}
