package tpd;

public class RainbowTask extends AbstractTask {
	
	private String	label;
	
	public RainbowTask ( String label ) {
		this.label = label;
	}
	
	public void doRainbow () {
		println("Rainbow task '" + label + "' is doing ..");
		printlnColor("Red");
		printlnColor("Orange");
		printlnColor("Yellow");
		printlnColor("Green");
		printlnColor("Blue");
		printlnColor("Indigo");
		println("Rainbow task '" + label + "' done.");
	}
	
}
